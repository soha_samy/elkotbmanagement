/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package elkotbmanagement;

/**
 *
 * @author EngSoha
 */
import java.awt.Font;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.format.VerticalAlignment;
import jxl.read.biff.BiffException;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableImage;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;

public class RollExcelReport {

    private WritableCellFormat times;
    private String inputFile;
    private String time;
    private String date;
    private String fileName;
    private String[] contain;
    private String[] containplace;
    private String[] containpoint;

    private void timeDate() {

        DateFormat df = new SimpleDateFormat("dd/MM/yy");
        Date dateobj = new Date();
        date = df.format(dateobj);
        df = new SimpleDateFormat("HH:mm:ss");
        Calendar calobj = Calendar.getInstance();
        time = df.format(calobj.getTime());

        String file1 = date.replace("/", " ");
        String file2 = time.replace(":", " ");

        fileName = file1 + file2;

    }

    private void arabicReadEdit() throws WriteException {

        File inputWorkbook = new File(inputFile);
        Workbook w;
        try {

            w = Workbook.getWorkbook(inputWorkbook);
            WritableWorkbook copy = Workbook.createWorkbook(new File("F:\\" + fileName + ".xls"), w);
            WritableSheet sheet2 = copy.getSheet(0);

            contain = null;
            contain = (String[]) mainInterface.customer.toArray(new String[mainInterface.customer.size()]);

            WritableFont fontCell = new WritableFont(WritableFont.TIMES, 12, WritableFont.BOLD);
            WritableCellFormat fontCellformat = new WritableCellFormat(fontCell);
            fontCellformat.setAlignment(Alignment.CENTRE);
            fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
            fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
            Label lbl = new Label(4, 1, contain[0], fontCellformat);
            sheet2.addCell(lbl);

            contain = null;
            contain = (String[]) mainInterface.date.toArray(new String[mainInterface.date.size()]);

            fontCell = new WritableFont(WritableFont.TIMES, 12, WritableFont.BOLD);
            fontCellformat = new WritableCellFormat(fontCell);
            fontCellformat.setAlignment(Alignment.CENTRE);
            fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
            fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
            lbl = new Label(4, 2, contain[0], fontCellformat);
            sheet2.addCell(lbl);

            contain = null;
            contain = (String[]) mainInterface.time.toArray(new String[mainInterface.time.size()]);

            fontCell = new WritableFont(WritableFont.TIMES, 12, WritableFont.BOLD);
            fontCellformat = new WritableCellFormat(fontCell);
            fontCellformat.setAlignment(Alignment.CENTRE);
            fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
            fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
            lbl = new Label(4, 3, contain[0], fontCellformat);
            sheet2.addCell(lbl);

            contain = null;
            contain = (String[]) mainInterface.appworker.toArray(new String[mainInterface.appworker.size()]);

            fontCell = new WritableFont(WritableFont.TIMES, 12, WritableFont.BOLD);
            fontCellformat = new WritableCellFormat(fontCell);
            fontCellformat.setAlignment(Alignment.CENTRE);
            fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
            fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
            lbl = new Label(4, 4, contain[0], fontCellformat);
            sheet2.addCell(lbl);

            contain = null;
            contain = (String[]) mainInterface.rollNo.toArray(new String[mainInterface.rollNo.size()]);

            fontCell = new WritableFont(WritableFont.TIMES, 12, WritableFont.BOLD);
            fontCellformat = new WritableCellFormat(fontCell);
            fontCellformat.setAlignment(Alignment.CENTRE);
            fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
            fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
            lbl = new Label(2, 8, contain[0], fontCellformat);
            sheet2.addCell(lbl);

            contain = null;
            contain = (String[]) mainInterface.patchNo.toArray(new String[mainInterface.patchNo.size()]);

            fontCell = new WritableFont(WritableFont.TIMES, 12, WritableFont.BOLD);
            fontCellformat = new WritableCellFormat(fontCell);
            fontCellformat.setAlignment(Alignment.CENTRE);
            fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
            fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
            lbl = new Label(3, 8, contain[0], fontCellformat);
            sheet2.addCell(lbl);

            contain = null;
            contain = (String[]) mainInterface.quality.toArray(new String[mainInterface.quality.size()]);

            fontCell = new WritableFont(WritableFont.TIMES, 12, WritableFont.BOLD);
            fontCellformat = new WritableCellFormat(fontCell);
            fontCellformat.setAlignment(Alignment.CENTRE);
            fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
            fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
            lbl = new Label(4, 8, contain[0], fontCellformat);
            sheet2.addCell(lbl);

            contain = null;
            contain = (String[]) mainInterface.defectNo.toArray(new String[mainInterface.defectNo.size()]);

            fontCell = new WritableFont(WritableFont.TIMES, 12, WritableFont.BOLD);
            fontCellformat = new WritableCellFormat(fontCell);
            fontCellformat.setAlignment(Alignment.CENTRE);
            fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
            fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
            lbl = new Label(5, 8, contain[0], fontCellformat);
            sheet2.addCell(lbl);

            contain = null;
            contain = (String[]) mainInterface.pointNo.toArray(new String[mainInterface.pointNo.size()]);

            fontCell = new WritableFont(WritableFont.TIMES, 12, WritableFont.BOLD);
            fontCellformat = new WritableCellFormat(fontCell);
            fontCellformat.setAlignment(Alignment.CENTRE);
            fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
            fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
            lbl = new Label(6, 8, contain[0], fontCellformat);
            sheet2.addCell(lbl);

            contain = null;
            contain = (String[]) mainInterface.standardColor.toArray(new String[mainInterface.standardColor.size()]);

            fontCell = new WritableFont(WritableFont.TIMES, 12, WritableFont.BOLD);
            fontCellformat = new WritableCellFormat(fontCell);
            fontCellformat.setAlignment(Alignment.CENTRE);
            fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
            fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
            lbl = new Label(7, 8, contain[0], fontCellformat);
            sheet2.addCell(lbl);

            contain = null;
            contain = (String[]) mainInterface.castra.toArray(new String[mainInterface.castra.size()]);

            fontCell = new WritableFont(WritableFont.TIMES, 12, WritableFont.BOLD);
            fontCellformat = new WritableCellFormat(fontCell);
            fontCellformat.setAlignment(Alignment.CENTRE);
            fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
            fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
            lbl = new Label(8, 8, contain[0], fontCellformat);
            sheet2.addCell(lbl);

            contain = null;
            contain = (String[]) mainInterface.knittingMach.toArray(new String[mainInterface.knittingMach.size()]);

            fontCell = new WritableFont(WritableFont.TIMES, 12, WritableFont.BOLD);
            fontCellformat = new WritableCellFormat(fontCell);
            fontCellformat.setAlignment(Alignment.CENTRE);
            fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
            fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
            lbl = new Label(2, 11, contain[0], fontCellformat);
            sheet2.addCell(lbl);

            contain = null;
            contain = (String[]) mainInterface.dyingMach.toArray(new String[mainInterface.dyingMach.size()]);

            fontCell = new WritableFont(WritableFont.TIMES, 12, WritableFont.BOLD);
            fontCellformat = new WritableCellFormat(fontCell);
            fontCellformat.setAlignment(Alignment.CENTRE);
            fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
            fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
            lbl = new Label(3, 11, contain[0], fontCellformat);
            sheet2.addCell(lbl);

            contain = null;
            contain = (String[]) mainInterface.type.toArray(new String[mainInterface.type.size()]);

            fontCell = new WritableFont(WritableFont.TIMES, 12, WritableFont.BOLD);
            fontCellformat = new WritableCellFormat(fontCell);
            fontCellformat.setAlignment(Alignment.CENTRE);
            fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
            fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
            lbl = new Label(4, 11, contain[0], fontCellformat);
            sheet2.addCell(lbl);

            contain = null;
            contain = (String[]) mainInterface.orderSample.toArray(new String[mainInterface.orderSample.size()]);

            fontCell = new WritableFont(WritableFont.TIMES, 12, WritableFont.BOLD);
            fontCellformat = new WritableCellFormat(fontCell);
            fontCellformat.setAlignment(Alignment.CENTRE);
            fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
            fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
            lbl = new Label(5, 11, contain[0], fontCellformat);
            sheet2.addCell(lbl);

            contain = null;
            contain = (String[]) mainInterface.qualitySys.toArray(new String[mainInterface.qualitySys.size()]);

            fontCell = new WritableFont(WritableFont.TIMES, 12, WritableFont.BOLD);
            fontCellformat = new WritableCellFormat(fontCell);
            fontCellformat.setAlignment(Alignment.CENTRE);
            fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
            fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
            lbl = new Label(6, 11, contain[0], fontCellformat);
            sheet2.addCell(lbl);

            contain = null;
            contain = (String[]) mainInterface.texture.toArray(new String[mainInterface.texture.size()]);

            fontCell = new WritableFont(WritableFont.TIMES, 12, WritableFont.BOLD);
            fontCellformat = new WritableCellFormat(fontCell);
            fontCellformat.setAlignment(Alignment.CENTRE);
            fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
            fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
            lbl = new Label(8, 11, contain[0], fontCellformat);
            sheet2.addCell(lbl);

            contain = null;
            contain = (String[]) mainInterface.standardWidth.toArray(new String[mainInterface.standardWidth.size()]);

            fontCell = new WritableFont(WritableFont.TIMES, 12, WritableFont.BOLD);
            fontCellformat = new WritableCellFormat(fontCell);
            fontCellformat.setAlignment(Alignment.CENTRE);
            fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
            fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
            lbl = new Label(2, 14, contain[0], fontCellformat);
            sheet2.addCell(lbl);

            contain = null;
            contain = (String[]) mainInterface.actualWidth.toArray(new String[mainInterface.actualWidth.size()]);

            fontCell = new WritableFont(WritableFont.TIMES, 12, WritableFont.BOLD);
            fontCellformat = new WritableCellFormat(fontCell);
            fontCellformat.setAlignment(Alignment.CENTRE);
            fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
            fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
            lbl = new Label(3, 14, contain[0], fontCellformat);
            sheet2.addCell(lbl);

            contain = null;
            contain = (String[]) mainInterface.length.toArray(new String[mainInterface.length.size()]);

            fontCell = new WritableFont(WritableFont.TIMES, 12, WritableFont.BOLD);
            fontCellformat = new WritableCellFormat(fontCell);
            fontCellformat.setAlignment(Alignment.CENTRE);
            fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
            fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
            lbl = new Label(4, 14, contain[0], fontCellformat);
            sheet2.addCell(lbl);

            contain = null;
            contain = (String[]) mainInterface.weight.toArray(new String[mainInterface.weight.size()]);

            fontCell = new WritableFont(WritableFont.TIMES, 12, WritableFont.BOLD);
            fontCellformat = new WritableCellFormat(fontCell);
            fontCellformat.setAlignment(Alignment.CENTRE);
            fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
            fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
            lbl = new Label(5, 14, contain[0], fontCellformat);
            sheet2.addCell(lbl);

            contain = null;
            contain = (String[]) mainInterface.weightMeter.toArray(new String[mainInterface.weightMeter.size()]);

            fontCell = new WritableFont(WritableFont.TIMES, 12, WritableFont.BOLD);
            fontCellformat = new WritableCellFormat(fontCell);
            fontCellformat.setAlignment(Alignment.CENTRE);
            fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
            fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
            lbl = new Label(6, 14, contain[0], fontCellformat);
            sheet2.addCell(lbl);

            contain = null;
            contain = (String[]) mainInterface.grossWeight.toArray(new String[mainInterface.grossWeight.size()]);

            fontCell = new WritableFont(WritableFont.TIMES, 12, WritableFont.BOLD);
            fontCellformat = new WritableCellFormat(fontCell);
            fontCellformat.setAlignment(Alignment.CENTRE);
            fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
            fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
            lbl = new Label(7, 14, contain[0], fontCellformat);
            sheet2.addCell(lbl);

            contain = null;
            contain = (String[]) mainInterface.actualCo.toArray(new String[mainInterface.actualCo.size()]);

            fontCell = new WritableFont(WritableFont.TIMES, 12, WritableFont.BOLD);
            fontCellformat = new WritableCellFormat(fontCell);
            fontCellformat.setAlignment(Alignment.CENTRE);
            fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
            fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
            lbl = new Label(8, 14, contain[0], fontCellformat);
            sheet2.addCell(lbl);

            contain = null;
            contain = (String[]) mainInterface.defect.get(0).toArray(new String[mainInterface.defect.get(0).size()]);
            containplace = (String[]) mainInterface.place.get(0).toArray(new String[mainInterface.place.get(0).size()]);
            containpoint = (String[]) mainInterface.points.get(0).toArray(new String[mainInterface.points.get(0).size()]);

            for (int i = 0; i < contain.length; i++) {

                if (contain[i] != "0") {

                    fontCell = new WritableFont(WritableFont.TIMES, 12, WritableFont.BOLD);
                    fontCellformat = new WritableCellFormat(fontCell);
                    fontCellformat.setAlignment(Alignment.CENTRE);
                    fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
                    fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
                    lbl = new Label(2, i + 18, contain[i], fontCellformat);
                    sheet2.addCell(lbl);
                    sheet2.mergeCells(2, i + 18, 3, i + 18);

                    fontCell = new WritableFont(WritableFont.TIMES, 12, WritableFont.BOLD);
                    fontCellformat = new WritableCellFormat(fontCell);
                    fontCellformat.setAlignment(Alignment.CENTRE);
                    fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
                    fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
                    lbl = new Label(4, i + 18, containpoint[i], fontCellformat);
                    sheet2.addCell(lbl);
                    sheet2.mergeCells(4, i + 18, 5, i + 18);

                    fontCell = new WritableFont(WritableFont.TIMES, 12, WritableFont.BOLD);
                    fontCellformat = new WritableCellFormat(fontCell);
                    fontCellformat.setAlignment(Alignment.CENTRE);
                    fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
                    fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
                    lbl = new Label(6, i + 18, containplace[i], fontCellformat);
                    sheet2.addCell(lbl);
                    sheet2.mergeCells(6, i + 18, 7, i + 18);
                }

            }

            copy.write();
            try {
                copy.close();
            } catch (WriteException ex) {
                Logger.getLogger(RollExcelReport.class.getName()).log(Level.SEVERE, null, ex);
            }
            w.close();

        } catch (IOException ex) {

            JLabel att = new JLabel("يجب أن تضع ملف يسمى rollReport في مسار المشروع");
            att.setFont(new Font("Serif", Font.BOLD, 20));
            JOptionPane.showMessageDialog(null, att, "انتباه !!", JOptionPane.INFORMATION_MESSAGE);

        } catch (BiffException ex) {
            Logger.getLogger(RollExcelReport.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

//    private void engReadEdit() throws WriteException {
//
//        File inputWorkbook = new File(inputFile);
//        Workbook w;
//        try {
//
//            w = Workbook.getWorkbook(inputWorkbook);
//            WritableWorkbook copy = Workbook.createWorkbook(new File("F:\\English\\" + fileName + ".xls"), w);
//            WritableSheet sheet2 = copy.getSheet(0);
//
//            WritableFont fontCell = new WritableFont(WritableFont.TIMES, 12, WritableFont.BOLD);
//            WritableCellFormat fontCellformat = new WritableCellFormat(fontCell);
//            fontCellformat.setAlignment(Alignment.CENTRE);
//            fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
//            fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
//            Label lbl = new Label(4, 1, Details.details[0], fontCellformat);
//            sheet2.addCell(lbl);
//
//            fontCell = new WritableFont(WritableFont.TIMES, 12, WritableFont.BOLD);
//            fontCellformat = new WritableCellFormat(fontCell);
//            fontCellformat.setAlignment(Alignment.CENTRE);
//            fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
//            fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
//            lbl = new Label(4, 2, date, fontCellformat);
//            sheet2.addCell(lbl);
//
//            fontCell = new WritableFont(WritableFont.TIMES, 12, WritableFont.BOLD);
//            fontCellformat = new WritableCellFormat(fontCell);
//            fontCellformat.setAlignment(Alignment.CENTRE);
//            fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
//            fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
//            lbl = new Label(4, 3, time, fontCellformat);
//            sheet2.addCell(lbl);
//
//            fontCell = new WritableFont(WritableFont.TIMES, 12, WritableFont.BOLD);
//            fontCellformat = new WritableCellFormat(fontCell);
//            fontCellformat.setAlignment(Alignment.CENTRE);
//            fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
//            fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
//            lbl = new Label(4, 4, "mohamed", fontCellformat);
//            sheet2.addCell(lbl);
//
//            fontCell = new WritableFont(WritableFont.TIMES, 12, WritableFont.BOLD);
//            fontCellformat = new WritableCellFormat(fontCell);
//            fontCellformat.setAlignment(Alignment.CENTRE);
//            fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
//            fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
//            lbl = new Label(2, 8, Details.details[3], fontCellformat);
//            sheet2.addCell(lbl);
//
//            fontCell = new WritableFont(WritableFont.TIMES, 12, WritableFont.BOLD);
//            fontCellformat = new WritableCellFormat(fontCell);
//            fontCellformat.setAlignment(Alignment.CENTRE);
//            fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
//            fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
//            lbl = new Label(3, 8, Details.details[2], fontCellformat);
//            sheet2.addCell(lbl);
//
//            fontCell = new WritableFont(WritableFont.TIMES, 12, WritableFont.BOLD);
//            fontCellformat = new WritableCellFormat(fontCell);
//            fontCellformat.setAlignment(Alignment.CENTRE);
//            fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
//            fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
//            lbl = new Label(4, 8, Details.quality, fontCellformat);
//            sheet2.addCell(lbl);
//
//            fontCell = new WritableFont(WritableFont.TIMES, 12, WritableFont.BOLD);
//            fontCellformat = new WritableCellFormat(fontCell);
//            fontCellformat.setAlignment(Alignment.CENTRE);
//            fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
//            fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
//            lbl = new Label(5, 8, String.valueOf(Details.currentDefect.length), fontCellformat);
//            sheet2.addCell(lbl);
//
//            fontCell = new WritableFont(WritableFont.TIMES, 12, WritableFont.BOLD);
//            fontCellformat = new WritableCellFormat(fontCell);
//            fontCellformat.setAlignment(Alignment.CENTRE);
//            fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
//            fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
//            lbl = new Label(6, 8, String.valueOf(Details.pointSum), fontCellformat);
//            sheet2.addCell(lbl);
//
//            fontCell = new WritableFont(WritableFont.TIMES, 12, WritableFont.BOLD);
//            fontCellformat = new WritableCellFormat(fontCell);
//            fontCellformat.setAlignment(Alignment.CENTRE);
//            fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
//            fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
//            lbl = new Label(7, 8, Details.details[1], fontCellformat);
//            sheet2.addCell(lbl);
//
//            fontCell = new WritableFont(WritableFont.TIMES, 12, WritableFont.BOLD);
//            fontCellformat = new WritableCellFormat(fontCell);
//            fontCellformat.setAlignment(Alignment.CENTRE);
//            fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
//            fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
//            lbl = new Label(8, 8, Details.details[13], fontCellformat);
//            sheet2.addCell(lbl);
//
//            fontCell = new WritableFont(WritableFont.TIMES, 12, WritableFont.BOLD);
//            fontCellformat = new WritableCellFormat(fontCell);
//            fontCellformat.setAlignment(Alignment.CENTRE);
//            fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
//            fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
//            lbl = new Label(2, 11, Details.details[7], fontCellformat);
//            sheet2.addCell(lbl);
//
//            fontCell = new WritableFont(WritableFont.TIMES, 12, WritableFont.BOLD);
//            fontCellformat = new WritableCellFormat(fontCell);
//            fontCellformat.setAlignment(Alignment.CENTRE);
//            fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
//            fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
//            lbl = new Label(3, 11, Details.details[8], fontCellformat);
//            sheet2.addCell(lbl);
//
//            fontCell = new WritableFont(WritableFont.TIMES, 12, WritableFont.BOLD);
//            fontCellformat = new WritableCellFormat(fontCell);
//            fontCellformat.setAlignment(Alignment.CENTRE);
//            fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
//            fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
//            lbl = new Label(4, 11, Details.details[9], fontCellformat);
//            sheet2.addCell(lbl);
//
//            fontCell = new WritableFont(WritableFont.TIMES, 12, WritableFont.BOLD);
//            fontCellformat = new WritableCellFormat(fontCell);
//            fontCellformat.setAlignment(Alignment.CENTRE);
//            fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
//            fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
//            lbl = new Label(5, 11, Details.details[10], fontCellformat);
//            sheet2.addCell(lbl);
//
//            fontCell = new WritableFont(WritableFont.TIMES, 12, WritableFont.BOLD);
//            fontCellformat = new WritableCellFormat(fontCell);
//            fontCellformat.setAlignment(Alignment.CENTRE);
//            fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
//            fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
//            lbl = new Label(6, 11, Details.details[14], fontCellformat);
//            sheet2.addCell(lbl);
//
//            fontCell = new WritableFont(WritableFont.TIMES, 12, WritableFont.BOLD);
//            fontCellformat = new WritableCellFormat(fontCell);
//            fontCellformat.setAlignment(Alignment.CENTRE);
//            fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
//            fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
//            lbl = new Label(7, 11, Details.details[6], fontCellformat);
//            sheet2.addCell(lbl);
//
//            fontCell = new WritableFont(WritableFont.TIMES, 12, WritableFont.BOLD);
//            fontCellformat = new WritableCellFormat(fontCell);
//            fontCellformat.setAlignment(Alignment.CENTRE);
//            fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
//            fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
//            lbl = new Label(8, 11, Details.details[11], fontCellformat);
//            sheet2.addCell(lbl);
//
//            fontCell = new WritableFont(WritableFont.TIMES, 12, WritableFont.BOLD);
//            fontCellformat = new WritableCellFormat(fontCell);
//            fontCellformat.setAlignment(Alignment.CENTRE);
//            fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
//            fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
//            lbl = new Label(2, 14, Details.details[4], fontCellformat);
//            sheet2.addCell(lbl);
//
//            fontCell = new WritableFont(WritableFont.TIMES, 12, WritableFont.BOLD);
//            fontCellformat = new WritableCellFormat(fontCell);
//            fontCellformat.setAlignment(Alignment.CENTRE);
//            fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
//            fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
//            lbl = new Label(3, 14, Details.details[5], fontCellformat);
//            sheet2.addCell(lbl);
//
//            fontCell = new WritableFont(WritableFont.TIMES, 12, WritableFont.BOLD);
//            fontCellformat = new WritableCellFormat(fontCell);
//            fontCellformat.setAlignment(Alignment.CENTRE);
//            fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
//            fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
//            lbl = new Label(4, 14, String.valueOf(Details.length), fontCellformat);
//            sheet2.addCell(lbl);
//
//            fontCell = new WritableFont(WritableFont.TIMES, 12, WritableFont.BOLD);
//            fontCellformat = new WritableCellFormat(fontCell);
//            fontCellformat.setAlignment(Alignment.CENTRE);
//            fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
//            fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
//            lbl = new Label(5, 14, String.valueOf(Details.weigtMach), fontCellformat);
//            sheet2.addCell(lbl);
//
//            fontCell = new WritableFont(WritableFont.TIMES, 12, WritableFont.BOLD);
//            fontCellformat = new WritableCellFormat(fontCell);
//            fontCellformat.setAlignment(Alignment.CENTRE);
//            fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
//            fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
//            lbl = new Label(6, 14, String.valueOf(Details.weigtMach / Details.length), fontCellformat);
//            sheet2.addCell(lbl);
//
//            fontCell = new WritableFont(WritableFont.TIMES, 12, WritableFont.BOLD);
//            fontCellformat = new WritableCellFormat(fontCell);
//            fontCellformat.setAlignment(Alignment.CENTRE);
//            fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
//            fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
//            lbl = new Label(7, 14, String.valueOf(FourPointReport.grossWeight), fontCellformat);
//            sheet2.addCell(lbl);
//
//            fontCell = new WritableFont(WritableFont.TIMES, 12, WritableFont.BOLD);
//            fontCellformat = new WritableCellFormat(fontCell);
//            fontCellformat.setAlignment(Alignment.CENTRE);
//            fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
//            fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
//            lbl = new Label(8, 14, Details.details[12], fontCellformat);
//            sheet2.addCell(lbl);
//
//            for (int i = 0; i < Details.currentDefect.length; i++) {
//
//                if (Details.currentDefect[i][0] != "0") {
//
//                    fontCell = new WritableFont(WritableFont.TIMES, 12, WritableFont.BOLD);
//                    fontCellformat = new WritableCellFormat(fontCell);
//                    fontCellformat.setAlignment(Alignment.CENTRE);
//                    fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
//                    fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
//                    lbl = new Label(2, i + 18, Details.currentDefect[i][3], fontCellformat);
//                    sheet2.addCell(lbl);
//                    sheet2.mergeCells(2, i + 18, 3, i + 18);
//
//                    fontCell = new WritableFont(WritableFont.TIMES, 12, WritableFont.BOLD);
//                    fontCellformat = new WritableCellFormat(fontCell);
//                    fontCellformat.setAlignment(Alignment.CENTRE);
//                    fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
//                    fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
//                    lbl = new Label(4, i + 18, Details.currentDefect[i][1], fontCellformat);
//                    sheet2.addCell(lbl);
//                    sheet2.mergeCells(4, i + 18, 5, i + 18);
//
//                    fontCell = new WritableFont(WritableFont.TIMES, 12, WritableFont.BOLD);
//                    fontCellformat = new WritableCellFormat(fontCell);
//                    fontCellformat.setAlignment(Alignment.CENTRE);
//                    fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
//                    fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
//                    lbl = new Label(6, i + 18, Details.currentDefect[i][2], fontCellformat);
//                    sheet2.addCell(lbl);
//                    sheet2.mergeCells(6, i + 18, 7, i + 18);
//                }
//
//            }
//
//            copy.write();
//            try {
//                copy.close();
//
//            } catch (WriteException ex) {
//                Logger.getLogger(ExcelReport.class
//                        .getName()).log(Level.SEVERE, null, ex);
//            }
//            w.close();
//
//        } catch (IOException ex) {
//
//            JLabel att = new JLabel("يجب أن تضع ملف يسمى rollReport في مسار المشروع");
//            att.setFont(new Font("Serif", Font.BOLD, 20));
//            JOptionPane.showMessageDialog(null, att, "انتباه !!", JOptionPane.INFORMATION_MESSAGE);
//
//        } catch (BiffException ex) {
//            Logger.getLogger(ExcelReport.class
//                    .getName()).log(Level.SEVERE, null, ex);
//        }
//
//    }

    public void main() throws WriteException {

        //ExcelReport report = new ExcelReport();
        timeDate();
        inputFile = "F:\\rollReport.xls";
        arabicReadEdit();
//        report.inputFile = "F:\\EnRollReport.xls";
//        report.engReadEdit();
    }

}
