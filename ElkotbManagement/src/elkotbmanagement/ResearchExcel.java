/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package elkotbmanagement;

/**
 *
 * @author EngSoha
 */
import java.awt.Font;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.format.VerticalAlignment;
import jxl.read.biff.BiffException;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableImage;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;

public class ResearchExcel {

    private static String inputFile = "F:\\SearchResult.xls";
    private static String time;
    private static String date;
    private static String fileName;
    String[] contain;
    static Workbook w;
    static WritableWorkbook copy;
    static WritableSheet sheet2;

    private void timeDate() {

        DateFormat df = new SimpleDateFormat("dd/MM/yy");
        Date dateobj = new Date();
        date = df.format(dateobj);
        df = new SimpleDateFormat("HH:mm:ss");
        Calendar calobj = Calendar.getInstance();
        time = df.format(calobj.getTime());

        String file1 = date.replace("/", " ");
        String file2 = time.replace(":", " ");

        fileName = file1 + file2;

    }

    protected void open() throws IOException, BiffException {

        timeDate();

        File inputWorkbook = new File(inputFile);

        w = Workbook.getWorkbook(inputWorkbook);
        copy = Workbook.createWorkbook(new File("F:\\" + fileName + ".xls"), w);
        sheet2 = copy.getSheet(0);
    }

    protected void writeDetails() throws IOException, BiffException, WriteException {

        int x = 0;

        contain = null;
        contain = (String[]) mainInterface.date.toArray(new String[mainInterface.date.size()]);

        for (int i = 0; i < mainInterface.date.size(); i++) {
            WritableFont fontCell = new WritableFont(WritableFont.TIMES, 12, WritableFont.BOLD);
            WritableCellFormat fontCellformat = new WritableCellFormat(fontCell);
            fontCellformat.setAlignment(Alignment.CENTRE);
            fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
            fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
            Label lbl = new Label(1, i + 3 + x, contain[i], fontCellformat);
            sheet2.addCell(lbl);
            sheet2.mergeCells(1, i + 3 + x, 1, i + 5 + x);

            x = x + 2;

        }

        contain = null;

        contain = (String[]) mainInterface.time.toArray(new String[mainInterface.time.size()]);

        x = 0;

        for (int i = 0; i < mainInterface.time.size(); i++) {
            WritableFont fontCell = new WritableFont(WritableFont.TIMES, 12, WritableFont.BOLD);
            WritableCellFormat fontCellformat = new WritableCellFormat(fontCell);
            fontCellformat.setAlignment(Alignment.CENTRE);
            fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
            fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
            Label lbl = new Label(2, i + 3 + x, contain[i], fontCellformat);
            sheet2.addCell(lbl);
            sheet2.mergeCells(2, i + 3 + x, 2, i + 5 + x);

            x = x + 2;

        }

        contain = null;

        contain = (String[]) mainInterface.rollNo.toArray(new String[mainInterface.rollNo.size()]);

        x = 0;

        for (int i = 0; i < mainInterface.rollNo.size(); i++) {
            WritableFont fontCell = new WritableFont(WritableFont.TIMES, 12, WritableFont.BOLD);
            WritableCellFormat fontCellformat = new WritableCellFormat(fontCell);
            fontCellformat.setAlignment(Alignment.CENTRE);
            fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
            fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
            Label lbl = new Label(3, i + 3 + x, contain[i], fontCellformat);
            sheet2.addCell(lbl);
            sheet2.mergeCells(3, i + 3 + x, 3, i + 5 + x);

            x = x + 2;

        }

        contain = null;

        contain = (String[]) mainInterface.patchNo.toArray(new String[mainInterface.patchNo.size()]);

        x = 0;

        for (int i = 0; i < mainInterface.patchNo.size(); i++) {
            WritableFont fontCell = new WritableFont(WritableFont.TIMES, 12, WritableFont.BOLD);
            WritableCellFormat fontCellformat = new WritableCellFormat(fontCell);
            fontCellformat.setAlignment(Alignment.CENTRE);
            fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
            fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
            Label lbl = new Label(4, i + 3 + x, contain[i], fontCellformat);
            sheet2.addCell(lbl);
            sheet2.mergeCells(4, i + 3 + x, 4, i + 5 + x);

            x = x + 2;

        }

        contain = null;
        contain = (String[]) mainInterface.customer.toArray(new String[mainInterface.customer.size()]);

        x = 0;

        for (int i = 0; i < mainInterface.customer.size(); i++) {
            WritableFont fontCell = new WritableFont(WritableFont.TIMES, 12, WritableFont.BOLD);
            WritableCellFormat fontCellformat = new WritableCellFormat(fontCell);
            fontCellformat.setAlignment(Alignment.CENTRE);
            fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
            fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
            Label lbl = new Label(5, i + 3 + x, contain[i], fontCellformat);
            sheet2.addCell(lbl);
            sheet2.mergeCells(5, i + 3 + x, 5, i + 5 + x);

            x = x + 2;

            System.out.println(contain[i]);

        }

        contain = null;

        contain = (String[]) mainInterface.standardColor.toArray(new String[mainInterface.standardColor.size()]);

        x = 0;

        for (int i = 0; i < mainInterface.standardColor.size(); i++) {
            WritableFont fontCell = new WritableFont(WritableFont.TIMES, 12, WritableFont.BOLD);
            WritableCellFormat fontCellformat = new WritableCellFormat(fontCell);
            fontCellformat.setAlignment(Alignment.CENTRE);
            fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
            fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
            Label lbl = new Label(6, i + 3 + x, contain[i], fontCellformat);
            sheet2.addCell(lbl);
            sheet2.mergeCells(6, i + 3 + x, 6, i + 5 + x);

            x = x + 2;

        }

        contain = null;

        contain = (String[]) mainInterface.type.toArray(new String[mainInterface.type.size()]);

        x = 0;

        for (int i = 0; i < mainInterface.type.size(); i++) {
            WritableFont fontCell = new WritableFont(WritableFont.TIMES, 12, WritableFont.BOLD);
            WritableCellFormat fontCellformat = new WritableCellFormat(fontCell);
            fontCellformat.setAlignment(Alignment.CENTRE);
            fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
            fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
            Label lbl = new Label(7, i + 3 + x, contain[i], fontCellformat);
            sheet2.addCell(lbl);
            sheet2.mergeCells(7, i + 3 + x, 7, i + 5 + x);

            x = x + 2;

        }

        contain = null;

        contain = (String[]) mainInterface.quality.toArray(new String[mainInterface.quality.size()]);

        x = 0;

        for (int i = 0; i < mainInterface.quality.size(); i++) {
            WritableFont fontCell = new WritableFont(WritableFont.TIMES, 12, WritableFont.BOLD);
            WritableCellFormat fontCellformat = new WritableCellFormat(fontCell);
            fontCellformat.setAlignment(Alignment.CENTRE);
            fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
            fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
            Label lbl = new Label(8, i + 3 + x, contain[i], fontCellformat);
            sheet2.addCell(lbl);
            sheet2.mergeCells(8, i + 3 + x, 8, i + 5 + x);

            x = x + 2;

        }

        contain = null;

        contain = (String[]) mainInterface.defectNo.toArray(new String[mainInterface.defectNo.size()]);

        x = 0;

        for (int i = 0; i < mainInterface.defectNo.size(); i++) {
            WritableFont fontCell = new WritableFont(WritableFont.TIMES, 12, WritableFont.BOLD);
            WritableCellFormat fontCellformat = new WritableCellFormat(fontCell);
            fontCellformat.setAlignment(Alignment.CENTRE);
            fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
            fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
            Label lbl = new Label(9, i + 3 + x, contain[i], fontCellformat);
            sheet2.addCell(lbl);
            sheet2.mergeCells(9, i + 3 + x, 9, i + 5 + x);

            x = x + 2;

        }

        contain = null;

        contain = (String[]) mainInterface.pointNo.toArray(new String[mainInterface.pointNo.size()]);

        x = 0;

        for (int i = 0; i < mainInterface.pointNo.size(); i++) {
            WritableFont fontCell = new WritableFont(WritableFont.TIMES, 12, WritableFont.BOLD);
            WritableCellFormat fontCellformat = new WritableCellFormat(fontCell);
            fontCellformat.setAlignment(Alignment.CENTRE);
            fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
            fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
            Label lbl = new Label(10, i + 3 + x, contain[i], fontCellformat);
            sheet2.addCell(lbl);
            sheet2.mergeCells(10, i + 3 + x, 10, i + 5 + x);

            x = x + 2;

        }

        contain = null;

        contain = (String[]) mainInterface.length.toArray(new String[mainInterface.length.size()]);

        x = 0;

        for (int i = 0; i < mainInterface.length.size(); i++) {
            WritableFont fontCell = new WritableFont(WritableFont.TIMES, 12, WritableFont.BOLD);
            WritableCellFormat fontCellformat = new WritableCellFormat(fontCell);
            fontCellformat.setAlignment(Alignment.CENTRE);
            fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
            fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
            Label lbl = new Label(42, i + 3 + x, contain[i], fontCellformat);
            sheet2.addCell(lbl);
            sheet2.mergeCells(42, i + 3 + x, 42, i + 5 + x);

            x = x + 2;

        }

        contain = null;

        contain = (String[]) mainInterface.length.toArray(new String[mainInterface.length.size()]);

        x = 0;

        for (int i = 0; i < mainInterface.rollNo.size(); i++) {
            WritableFont fontCell = new WritableFont(WritableFont.TIMES, 12, WritableFont.BOLD);
            WritableCellFormat fontCellformat = new WritableCellFormat(fontCell);
            fontCellformat.setAlignment(Alignment.CENTRE);
            fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
            fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
            Label lbl = new Label(43,i + 3 + x, contain[i], fontCellformat);
            sheet2.addCell(lbl);
            sheet2.mergeCells(43, i + 3 + x, 43, i + 5 + x);

            x = x + 2;

        }

        contain = null;

        contain = (String[]) mainInterface.weight.toArray(new String[mainInterface.weight.size()]);
        x = 0;

        for (int i = 0; i < mainInterface.weight.size(); i++) {
            WritableFont fontCell = new WritableFont(WritableFont.TIMES, 12, WritableFont.BOLD);
            WritableCellFormat fontCellformat = new WritableCellFormat(fontCell);
            fontCellformat.setAlignment(Alignment.CENTRE);
            fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
            fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
            Label lbl = new Label(44, i + 3 + x, contain[i], fontCellformat);
            sheet2.addCell(lbl);
            sheet2.mergeCells(44, i + 3 + x, 44, i + 5 + x);

            x = x + 2;

        }
        contain = null;

        contain = (String[]) mainInterface.weightMeter.toArray(new String[mainInterface.weightMeter.size()]);

        x = 0;

        for (int i = 0; i < mainInterface.weightMeter.size(); i++) {
            WritableFont fontCell = new WritableFont(WritableFont.TIMES, 12, WritableFont.BOLD);
            WritableCellFormat fontCellformat = new WritableCellFormat(fontCell);
            fontCellformat.setAlignment(Alignment.CENTRE);
            fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
            fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
            Label lbl = new Label(45, i + 3 + x, contain[i], fontCellformat);
            sheet2.addCell(lbl);
            sheet2.mergeCells(45, i + 3 + x, 45, i + 5 + x);

            x = x + 2;

        }
        contain = null;
        contain = (String[]) mainInterface.grossWeight.toArray(new String[mainInterface.grossWeight.size()]);

        x = 0;

        for (int i = 0; i < mainInterface.grossWeight.size(); i++) {
            WritableFont fontCell = new WritableFont(WritableFont.TIMES, 12, WritableFont.BOLD);
            WritableCellFormat fontCellformat = new WritableCellFormat(fontCell);
            fontCellformat.setAlignment(Alignment.CENTRE);
            fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
            fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
            Label lbl = new Label(46, i + 3 + x, contain[i], fontCellformat);
            sheet2.addCell(lbl);
            sheet2.mergeCells(46, i + 3 + x, 46, i + 5 + x);

            x = x + 2;

        }
        contain = null;
        contain = (String[]) mainInterface.qualitySys.toArray(new String[mainInterface.qualitySys.size()]);

        x = 0;

        for (int i = 0; i < mainInterface.qualitySys.size(); i++) {
            WritableFont fontCell = new WritableFont(WritableFont.TIMES, 12, WritableFont.BOLD);
            WritableCellFormat fontCellformat = new WritableCellFormat(fontCell);
            fontCellformat.setAlignment(Alignment.CENTRE);
            fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
            fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
            Label lbl = new Label(47, i + 3 + x, contain[i], fontCellformat);
            sheet2.addCell(lbl);
            sheet2.mergeCells(47, i + 3 + x, 47, i + 5 + x);

            x = x + 2;

        }
        contain = null;

        contain = (String[]) mainInterface.appworker.toArray(new String[mainInterface.appworker.size()]);

        x = 0;

        for (int i = 0; i < mainInterface.appworker.size(); i++) {
            WritableFont fontCell = new WritableFont(WritableFont.TIMES, 12, WritableFont.BOLD);
            WritableCellFormat fontCellformat = new WritableCellFormat(fontCell);
            fontCellformat.setAlignment(Alignment.CENTRE);
            fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
            fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
            Label lbl = new Label(48, i + 3 + x, contain[i], fontCellformat);
            sheet2.addCell(lbl);
            sheet2.mergeCells(48, i + 3 + x, 48, i + 5 + x);

            x = x + 2;

        }
        contain = null;

        contain = (String[]) mainInterface.knittingMach.toArray(new String[mainInterface.knittingMach.size()]);

        x = 0;

        for (int i = 0; i < mainInterface.knittingMach.size(); i++) {
            WritableFont fontCell = new WritableFont(WritableFont.TIMES, 12, WritableFont.BOLD);
            WritableCellFormat fontCellformat = new WritableCellFormat(fontCell);
            fontCellformat.setAlignment(Alignment.CENTRE);
            fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
            fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
            Label lbl = new Label(49, i + 3 + x, contain[i], fontCellformat);
            sheet2.addCell(lbl);
            sheet2.mergeCells(49, i + 3 + x, 49, i + 5 + x);

            x = x + 2;

        }
        contain = null;

        contain = (String[]) mainInterface.dyingMach.toArray(new String[mainInterface.dyingMach.size()]);

        x = 0;

        for (int i = 0; i < mainInterface.dyingMach.size(); i++) {
            WritableFont fontCell = new WritableFont(WritableFont.TIMES, 12, WritableFont.BOLD);
            WritableCellFormat fontCellformat = new WritableCellFormat(fontCell);
            fontCellformat.setAlignment(Alignment.CENTRE);
            fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
            fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
            Label lbl = new Label(50, i + 3 + x, contain[i], fontCellformat);
            sheet2.addCell(lbl);
            sheet2.mergeCells(50, i + 3 + x, 50, i + 5 + x);

            x = x + 2;

        }
        contain = null;

        contain = (String[]) mainInterface.texture.toArray(new String[mainInterface.texture.size()]);

        x = 0;

        for (int i = 0; i < mainInterface.texture.size(); i++) {
            WritableFont fontCell = new WritableFont(WritableFont.TIMES, 12, WritableFont.BOLD);
            WritableCellFormat fontCellformat = new WritableCellFormat(fontCell);
            fontCellformat.setAlignment(Alignment.CENTRE);
            fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
            fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
            Label lbl = new Label(51, i + 3 + x, contain[i], fontCellformat);
            sheet2.addCell(lbl);
            sheet2.mergeCells(51, i + 3 + x, 51, i + 5 + x);

            x = x + 2;

        }
        contain = null;

        contain = (String[]) mainInterface.actualCo.toArray(new String[mainInterface.actualCo.size()]);

        x = 0;

        for (int i = 0; i < mainInterface.actualCo.size(); i++) {
            WritableFont fontCell = new WritableFont(WritableFont.TIMES, 12, WritableFont.BOLD);
            WritableCellFormat fontCellformat = new WritableCellFormat(fontCell);
            fontCellformat.setAlignment(Alignment.CENTRE);
            fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
            fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
            Label lbl = new Label(52, i + 3 + x, contain[i], fontCellformat);
            sheet2.addCell(lbl);
            sheet2.mergeCells(52, i + 3 + x, 52, i + 5 + x);

            x = x + 2;

        }
        contain = null;

        contain = (String[]) mainInterface.castra.toArray(new String[mainInterface.castra.size()]);

        x = 0;

        for (int i = 0; i < mainInterface.castra.size(); i++) {
            WritableFont fontCell = new WritableFont(WritableFont.TIMES, 12, WritableFont.BOLD);
            WritableCellFormat fontCellformat = new WritableCellFormat(fontCell);
            fontCellformat.setAlignment(Alignment.CENTRE);
            fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
            fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
            Label lbl = new Label(53, i + 3 + x, contain[i], fontCellformat);
            sheet2.addCell(lbl);
            sheet2.mergeCells(53, i + 3 + x, 53, i + 5 + x);

            x = x + 2;

        }
        contain = null;

        copy.write();
        copy.close();
        w.close();

    }

    protected void writeDefect() throws IOException, BiffException, WriteException {

        int x = 0;

        for (int i = 0; i < mainInterface.defect.size(); i++) {

            contain = (String[]) mainInterface.defect.get(i).toArray(new String[mainInterface.defect.get(i).size()]);

            for (int j = 0; j < contain.length; j++) {
                WritableFont fontCell = new WritableFont(WritableFont.TIMES, 12, WritableFont.BOLD);
                WritableCellFormat fontCellformat = new WritableCellFormat(fontCell);
                fontCellformat.setAlignment(Alignment.CENTRE);
                fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
                fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
                Label lbl = new Label(j + 11, i + 3 + x, contain[j], fontCellformat);
                sheet2.addCell(lbl);

            }

            x = x + 2;

        }

        contain = null;

    }

    protected void writePlace() throws IOException, BiffException, WriteException {

        int x = 0;

        for (int i = 0; i < mainInterface.place.size(); i++) {

            contain = (String[]) mainInterface.place.get(i).toArray(new String[mainInterface.place.get(i).size()]);

            for (int j = 0; j < contain.length; j++) {
                WritableFont fontCell = new WritableFont(WritableFont.TIMES, 12, WritableFont.BOLD);
                WritableCellFormat fontCellformat = new WritableCellFormat(fontCell);
                fontCellformat.setAlignment(Alignment.CENTRE);
                fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
                fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
                Label lbl = new Label(j + 11, i + 5 + x, contain[j], fontCellformat);
                sheet2.addCell(lbl);

            }

            x = x + 2;

        }

        contain = null;

    }

    protected void writePoint() throws IOException, BiffException, WriteException {

        int x = 0;

        for (int i = 0; i < mainInterface.points.size(); i++) {

            contain = (String[]) mainInterface.points.get(i).toArray(new String[mainInterface.points.get(i).size()]);

            for (int j = 0; j < contain.length; j++) {
                WritableFont fontCell = new WritableFont(WritableFont.TIMES, 12, WritableFont.BOLD);
                WritableCellFormat fontCellformat = new WritableCellFormat(fontCell);
                fontCellformat.setAlignment(Alignment.CENTRE);
                fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
                fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
                Label lbl = new Label(j + 11, i + 4 + x, contain[j], fontCellformat);
                sheet2.addCell(lbl);

            }

            x = x + 2;

        }

        contain = null;

    }

    public void main() throws WriteException, IOException, BiffException {

        
        inputFile = "F:\\SearchResult.xls";

        open();
        writePoint();
        writePlace();
        writeDefect();
        writeDetails();


    }

}
