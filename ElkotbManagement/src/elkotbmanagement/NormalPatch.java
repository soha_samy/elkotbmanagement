/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package elkotbmanagement;

/**
 *
 * @author EngSoha
 */
import java.awt.Font;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.format.VerticalAlignment;
import jxl.read.biff.BiffException;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableImage;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;

public class NormalPatch {

    private static String inputFile = "F:\\SearchResult.xls";
    private static String time;
    private static String date;
    private static String fileName;
    String[] contain;
    static Workbook w;
    static WritableWorkbook copy;
    static WritableSheet sheet2;

    private static final double CELL_DEFAULT_HEIGHT = 50;
    private static final double CELL_DEFAULT_WIDTH = 30;

    private void timeDate() {

        DateFormat df = new SimpleDateFormat("dd/MM/yy");
        Date dateobj = new Date();
        date = df.format(dateobj);
        df = new SimpleDateFormat("HH:mm:ss");
        Calendar calobj = Calendar.getInstance();
        time = df.format(calobj.getTime());

        String file1 = date.replace("/", " ");
        String file2 = time.replace(":", " ");

        fileName = file1 + file2;

    }

    protected void open() throws IOException, BiffException {

        timeDate();

        File inputWorkbook = new File(inputFile);

        w = Workbook.getWorkbook(inputWorkbook);
        copy = Workbook.createWorkbook(new File("F:\\" + fileName + ".xls"), w);
        sheet2 = copy.getSheet(0);
    }

    private void writeReport() throws WriteException, IOException {

        if (mainInterface.production.equals("kotb")) {

            File imageFile = new File("F:\\ElkotbBackground.jpg");
            BufferedImage input = ImageIO.read(imageFile);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();

            ImageIO.write(input,
                    "jpg", baos);
            sheet2.addImage(
                    new WritableImage(1, 1, input.getWidth() / CELL_DEFAULT_WIDTH,
                            input.getHeight() / CELL_DEFAULT_HEIGHT, baos.toByteArray()));
        } else {

            File imageFile = new File("F:\\ElkotbBackground.jpg");
            BufferedImage input = ImageIO.read(imageFile);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();

            ImageIO.write(input,
                    "jpg", baos);
            sheet2.addImage(
                    new WritableImage(1, 1, input.getWidth() / CELL_DEFAULT_WIDTH,
                            input.getHeight() / CELL_DEFAULT_HEIGHT, baos.toByteArray()));

            imageFile = new File("F:\\printPlus.jpg");
            input = ImageIO.read(imageFile);
            baos = new ByteArrayOutputStream();

            ImageIO.write(input,
                    "jpg", baos);
            sheet2.addImage(
                    new WritableImage(1, 3, input.getWidth() / CELL_DEFAULT_WIDTH,
                            input.getHeight() / CELL_DEFAULT_HEIGHT, baos.toByteArray()));

        }

        contain = (String[]) mainInterface.date.toArray(new String[mainInterface.date.size()]);

        WritableFont fontCell = new WritableFont(WritableFont.TIMES, 10, WritableFont.BOLD);
        WritableCellFormat fontCellformat = new WritableCellFormat(fontCell);
        fontCellformat.setAlignment(Alignment.CENTRE);
        fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
        fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
        Label lbl = new Label(25, 1, contain[0] + "--" + contain[contain.length - 1], fontCellformat);
        sheet2.addCell(lbl);
        sheet2.mergeCells(25, 1, 27, 1);

        contain = null;
        contain = (String[]) mainInterface.time.toArray(new String[mainInterface.time.size()]);

        fontCell = new WritableFont(WritableFont.TIMES, 10, WritableFont.BOLD);
        fontCellformat = new WritableCellFormat(fontCell);
        fontCellformat.setAlignment(Alignment.CENTRE);
        fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
        fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
        lbl = new Label(25, 2, contain[0] + "--" + contain[contain.length - 1], fontCellformat);
        sheet2.addCell(lbl);
        sheet2.mergeCells(25, 2, 27, 2);

//        contain = null;
//        contain = (String[]) mainInterface.time.toArray(new String[mainInterface.time.size()]);
//
//        fontCell = new WritableFont(WritableFont.TIMES, 10, WritableFont.BOLD);
//        fontCellformat = new WritableCellFormat(fontCell);
//        fontCellformat.setAlignment(Alignment.CENTRE);
//        fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
//        fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
//        lbl = new Label(25, 2, contain[0] + "--" + contain[contain.length], fontCellformat);
//        sheet2.addCell(lbl);
//        sheet2.mergeCells(25, 2, 26, 2);
        contain = null;
        contain = (String[]) mainInterface.engAppworker.toArray(new String[mainInterface.engAppworker.size()]);

        fontCell = new WritableFont(WritableFont.TIMES, 10, WritableFont.BOLD);
        fontCellformat = new WritableCellFormat(fontCell);
        fontCellformat.setAlignment(Alignment.CENTRE);
        fontCellformat.setWrap(true);
        fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
        fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
        lbl = new Label(31, 1, contain[0], fontCellformat);
        sheet2.addCell(lbl);
        sheet2.mergeCells(31, 1, 33, 2);

        contain = null;
        contain = (String[]) mainInterface.customer.toArray(new String[mainInterface.customer.size()]);

        fontCell = new WritableFont(WritableFont.TIMES, 10, WritableFont.BOLD);
        fontCellformat = new WritableCellFormat(fontCell);
        fontCellformat.setAlignment(Alignment.CENTRE);
        fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
        fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
        lbl = new Label(3, 6, contain[0], fontCellformat);
        sheet2.addCell(lbl);
        sheet2.mergeCells(3, 6, 7, 6);

        contain = null;
        contain = (String[]) mainInterface.rollNo.toArray(new String[mainInterface.rollNo.size()]);

        fontCell = new WritableFont(WritableFont.TIMES, 10, WritableFont.BOLD);
        fontCellformat = new WritableCellFormat(fontCell);
        fontCellformat.setAlignment(Alignment.CENTRE);
        fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
        fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
        lbl = new Label(19, 6, String.valueOf(contain.length), fontCellformat);
        sheet2.addCell(lbl);
        sheet2.mergeCells(19, 6, 21, 6);

        /* contain = null;   for batch size
         contain = (String[]) mainInterface.rollNo.toArray(new String[mainInterface.rollNo.size()]);

         fontCell = new WritableFont(WritableFont.TIMES, 12, WritableFont.BOLD);
         fontCellformat = new WritableCellFormat(fontCell);
         fontCellformat.setAlignment(Alignment.CENTRE);
         fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
         fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
         lbl = new Label(19, 5, String.valueOf(contain.length), fontCellformat);
         sheet2.addCell(lbl);
         sheet2.mergeCells(19, 5, 19, 5);*/
 /*contain = null; for quality
         contain = (String[]) mainInterface.rollNo.toArray(new String[mainInterface.rollNo.size()]);

         fontCell = new WritableFont(WritableFont.TIMES, 12, WritableFont.BOLD);
         fontCellformat = new WritableCellFormat(fontCell);
         fontCellformat.setAlignment(Alignment.CENTRE);
         fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
         fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
         lbl = new Label(19, 5, String.valueOf(contain.length), fontCellformat);
         sheet2.addCell(lbl);
         sheet2.mergeCells(19, 5, 19, 5);*/
        contain = null;
        contain = (String[]) mainInterface.actualCo.toArray(new String[mainInterface.actualCo.size()]);

        fontCell = new WritableFont(WritableFont.TIMES, 10, WritableFont.BOLD);
        fontCellformat = new WritableCellFormat(fontCell);
        fontCellformat.setAlignment(Alignment.CENTRE);
        fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
        fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
        lbl = new Label(29, 6, contain[0], fontCellformat);
        sheet2.addCell(lbl);
        sheet2.mergeCells(29, 6, 30, 6);

        contain = null;
        contain = (String[]) mainInterface.patchNo.toArray(new String[mainInterface.patchNo.size()]);

        fontCell = new WritableFont(WritableFont.TIMES, 10, WritableFont.BOLD);
        fontCellformat = new WritableCellFormat(fontCell);
        fontCellformat.setAlignment(Alignment.CENTRE);
        fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
        fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
        lbl = new Label(31, 6, contain[0], fontCellformat);
        sheet2.addCell(lbl);
        sheet2.mergeCells(31, 6, 32, 7);

        contain = null;
        contain = (String[]) mainInterface.rollNo.toArray(new String[mainInterface.rollNo.size()]);
        int x = 0;
        int rolls = contain.length;

        for (int i = 0; i < contain.length; i++) {

            fontCell = new WritableFont(WritableFont.TIMES, 10, WritableFont.BOLD);
            fontCellformat = new WritableCellFormat(fontCell);
            fontCellformat.setAlignment(Alignment.CENTRE);
            fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
            fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
            lbl = new Label(1, i + 17 + x, contain[i], fontCellformat);
            sheet2.addCell(lbl);
            sheet2.mergeCells(1, i + 17 + x, 1, i + 18 + x);

            x = x + 1;

        }

        x = 0;

        for (int i = 0; i < mainInterface.defect.size(); i++) {

            String[] containPoint;
            String[] containPlace;
            int x1 = 0;
            int x2 = 0;
            int x3 = 0;
            int x4 = 0;
            int x0 = 0;
            int xalot = 0;

            // to arrange the roll]
            contain = null;
            containPoint = null;
            containPlace = null;

            contain = (String[]) mainInterface.defect.get(i).toArray(new String[mainInterface.defect.get(i).size()]);
            containPoint = (String[]) mainInterface.points.get(i).toArray(new String[mainInterface.points.get(i).size()]);
            containPlace = (String[]) mainInterface.place.get(i).toArray(new String[mainInterface.place.get(i).size()]);

            for (int j = 0; j < contain.length; j++) {
                // to arrange defects in each roll

                if (contain[j] != "0") {

                    if (containPoint[j].equals("1")) {
                        if (x1 < 6) {
                            fontCell = new WritableFont(WritableFont.TIMES, 10, WritableFont.BOLD);
                            fontCellformat = new WritableCellFormat(fontCell);
                            fontCellformat.setAlignment(Alignment.CENTRE);
                            fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
                            fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
                            lbl = new Label(2 + x1, i + 17 + x, contain[j] + " " + containPlace[j], fontCellformat);
                            sheet2.addCell(lbl);
                            x1++;
                        } else {

                            fontCell = new WritableFont(WritableFont.TIMES, 10, WritableFont.BOLD);
                            fontCellformat = new WritableCellFormat(fontCell);
                            fontCellformat.setAlignment(Alignment.CENTRE);
                            fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
                            fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
                            lbl = new Label(2 + x1, i + 18 + x, contain[j] + " " + containPlace[j], fontCellformat);
                            sheet2.addCell(lbl);
                            x1++;

                        }
                    }
                    if (containPoint[j].equals("2")) {
                        if (x2 < 6) {
                            fontCell = new WritableFont(WritableFont.TIMES, 10, WritableFont.BOLD);
                            fontCellformat = new WritableCellFormat(fontCell);
                            fontCellformat.setAlignment(Alignment.CENTRE);
                            fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
                            fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
                            lbl = new Label(8 + x2, i + 17 + x, contain[j] + " " + containPlace[j], fontCellformat);
                            sheet2.addCell(lbl);
                            x2++;
                        } else {

                            fontCell = new WritableFont(WritableFont.TIMES, 10, WritableFont.BOLD);
                            fontCellformat = new WritableCellFormat(fontCell);
                            fontCellformat.setAlignment(Alignment.CENTRE);
                            fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
                            fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
                            lbl = new Label(8 + x2, i + 18 + x, contain[j] + " " + containPlace[j], fontCellformat);
                            sheet2.addCell(lbl);
                            x2++;

                        }
                    }

                    if (containPoint[j].equals("3")) {
                        if (x3 < 6) {
                            fontCell = new WritableFont(WritableFont.TIMES, 10, WritableFont.BOLD);
                            fontCellformat = new WritableCellFormat(fontCell);
                            fontCellformat.setAlignment(Alignment.CENTRE);
                            fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
                            fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
                            lbl = new Label(14 + x3, i + 17 + x, contain[j] + " " + containPlace[j], fontCellformat);
                            sheet2.addCell(lbl);
                            x3++;
                        } else {

                            fontCell = new WritableFont(WritableFont.TIMES, 10, WritableFont.BOLD);
                            fontCellformat = new WritableCellFormat(fontCell);
                            fontCellformat.setAlignment(Alignment.CENTRE);
                            fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
                            fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
                            lbl = new Label(14 + x3, i + 18 + x, contain[j] + " " + containPlace[j], fontCellformat);
                            sheet2.addCell(lbl);
                            x3++;

                        }
                    }

                    if (containPoint[i].equals("4")) {
                        if (x4 < 6) {
                            fontCell = new WritableFont(WritableFont.TIMES, 10, WritableFont.BOLD);
                            fontCellformat = new WritableCellFormat(fontCell);
                            fontCellformat.setAlignment(Alignment.CENTRE);
                            fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
                            fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
                            lbl = new Label(20 + x4, i + 17 + x, contain[j] + " " + containPlace[j], fontCellformat);
                            sheet2.addCell(lbl);
                            x4++;
                        } else {

                            fontCell = new WritableFont(WritableFont.TIMES, 10, WritableFont.BOLD);
                            fontCellformat = new WritableCellFormat(fontCell);
                            fontCellformat.setAlignment(Alignment.CENTRE);
                            fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
                            fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
                            lbl = new Label(20 + x4, i + 18 + x, contain[j] + " " + containPlace[j], fontCellformat);
                            sheet2.addCell(lbl);
                            x4++;

                        }

                    }

                    if (containPoint[i].equals("0")) {
                        if (x0 < 6) {
                            fontCell = new WritableFont(WritableFont.TIMES, 10, WritableFont.BOLD);
                            fontCellformat = new WritableCellFormat(fontCell);
                            fontCellformat.setAlignment(Alignment.CENTRE);
                            fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
                            fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
                            lbl = new Label(26 + x0, i + 17 + x, contain[j] + " " + containPlace[j], fontCellformat);
                            sheet2.addCell(lbl);
                            x0++;
                        } else {

                            fontCell = new WritableFont(WritableFont.TIMES, 10, WritableFont.BOLD);
                            fontCellformat = new WritableCellFormat(fontCell);
                            fontCellformat.setAlignment(Alignment.CENTRE);
                            fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
                            fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
                            lbl = new Label(26 + x0, i + 18 + x, contain[j] + " " + containPlace[j], fontCellformat);
                            sheet2.addCell(lbl);
                            x0++;

                        }

                    }
                }
            }

            x = x + 1;

        }

        contain = null;

        contain = (String[]) mainInterface.standardWidth.toArray(new String[mainInterface.standardWidth.size()]);

        x = 0;

        for (int i = 0; i < mainInterface.standardWidth.size(); i++) {
            fontCell = new WritableFont(WritableFont.TIMES, 10, WritableFont.BOLD);
            fontCellformat = new WritableCellFormat(fontCell);
            fontCellformat.setAlignment(Alignment.CENTRE);
            fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
            fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
            lbl = new Label(32, i + 17 + x, contain[i], fontCellformat);
            sheet2.addCell(lbl);
            sheet2.mergeCells(32, i + 17 + x, 32, i + 18 + x);

            x = x + 1;

        }

        contain = null;

        contain = (String[]) mainInterface.actualWidth.toArray(new String[mainInterface.actualWidth.size()]);

        x = 0;

        for (int i = 0; i < mainInterface.actualWidth.size(); i++) {
            fontCell = new WritableFont(WritableFont.TIMES, 10, WritableFont.BOLD);
            fontCellformat = new WritableCellFormat(fontCell);
            fontCellformat.setAlignment(Alignment.CENTRE);
            fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
            fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
            lbl = new Label(33, i + 17 + x, contain[i], fontCellformat);
            sheet2.addCell(lbl);
            sheet2.mergeCells(33, i + 17 + x, 33, i + 18 + x);

            x = x + 1;

        }

        contain = null;

        contain = (String[]) mainInterface.length.toArray(new String[mainInterface.length.size()]);

        x = 0;

        for (int i = 0; i < mainInterface.length.size(); i++) {
            fontCell = new WritableFont(WritableFont.TIMES, 10, WritableFont.BOLD);
            fontCellformat = new WritableCellFormat(fontCell);
            fontCellformat.setAlignment(Alignment.CENTRE);
            fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
            fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
            lbl = new Label(34, i + 17 + x, contain[i], fontCellformat);
            sheet2.addCell(lbl);
            sheet2.mergeCells(34, i + 17 + x, 35, i + 18 + x);

            x = x + 1;

        }

        contain = null;

        contain = (String[]) mainInterface.weight.toArray(new String[mainInterface.weight.size()]);

        x = 0;

        for (int i = 0; i < mainInterface.weight.size(); i++) {
            fontCell = new WritableFont(WritableFont.TIMES, 10, WritableFont.BOLD);
            fontCellformat = new WritableCellFormat(fontCell);
            fontCellformat.setAlignment(Alignment.CENTRE);
            fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
            fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
            lbl = new Label(36, i + 17 + x, contain[i], fontCellformat);
            sheet2.addCell(lbl);
            sheet2.mergeCells(36, i + 17 + x, 37, i + 18 + x);

            x = x + 1;

        }

//        contain = null;
//
//        contain = (String[]) mainInterface.pointNo.toArray(new String[mainInterface.pointNo.size()]);
//
//        x = 0;
//
//        for (int i = 0; i < mainInterface.pointNo.size(); i++) {
//            fontCell = new WritableFont(WritableFont.TIMES, 10, WritableFont.BOLD);
//            fontCellformat = new WritableCellFormat(fontCell);
//            fontCellformat.setAlignment(Alignment.CENTRE);
//            fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
//            fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
//            lbl = new Label(38, i + 17 + x, contain[i], fontCellformat);
//            sheet2.addCell(lbl);
//            sheet2.mergeCells(38, i + 17 + x, 38, i + 18 + x);
//
//            x = x + 1;
//
//        }
        x = 0;

        for (int i = 0; i < mainInterface.defect.size(); i++) {

            fontCell = new WritableFont(WritableFont.TIMES, 10, WritableFont.BOLD);
            fontCellformat = new WritableCellFormat(fontCell);
            fontCellformat.setAlignment(Alignment.CENTRE);
            fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
            fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
            lbl = new Label(38, i + 17 + x, String.valueOf(mainInterface.defect.get(i).size()), fontCellformat);
            sheet2.addCell(lbl);
            sheet2.mergeCells(38, i + 17 + x, 38, i + 18 + x);

            x = x + 1;

        }

        contain = null;

        contain = (String[]) mainInterface.quality.toArray(new String[mainInterface.quality.size()]);

        x = 0;

        for (int i = 0; i < mainInterface.quality.size(); i++) {

            if (contain[i].equals("أولى")) {
                fontCell = new WritableFont(WritableFont.TIMES, 10, WritableFont.BOLD);
                fontCellformat = new WritableCellFormat(fontCell);
                fontCellformat.setAlignment(Alignment.CENTRE);
                fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
                fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
                lbl = new Label(39, i + 17 + x, "OK", fontCellformat);
                sheet2.addCell(lbl);
                sheet2.mergeCells(39, i + 17 + x, 39, i + 18 + x);
            } else if (contain[i].equals("ثانية")) {
                fontCell = new WritableFont(WritableFont.TIMES, 10, WritableFont.BOLD);
                fontCellformat = new WritableCellFormat(fontCell);
                fontCellformat.setAlignment(Alignment.CENTRE);
                fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
                fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
                lbl = new Label(39, i + 17 + x, "NOK", fontCellformat);
                sheet2.addCell(lbl);
                sheet2.mergeCells(39, i + 17 + x, 39, i + 18 + x);
            }

            x = x + 1;

        }

        fontCell = new WritableFont(WritableFont.TIMES, 11, WritableFont.BOLD);
        fontCellformat = new WritableCellFormat(fontCell);
        fontCellformat.setAlignment(Alignment.CENTRE);
        fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
        lbl = new Label(1, rolls + 17 + 2, "inspect result", fontCellformat);
        sheet2.addCell(lbl);

        fontCell = new WritableFont(WritableFont.TIMES, 11, WritableFont.BOLD);
        fontCellformat = new WritableCellFormat(fontCell);
        fontCellformat.setAlignment(Alignment.CENTRE);
        fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
        lbl = new Label(1, rolls + 17 + 3, "ok", fontCellformat);
        sheet2.addCell(lbl);

        fontCell = new WritableFont(WritableFont.TIMES, 11, WritableFont.BOLD);
        fontCellformat = new WritableCellFormat(fontCell);
        fontCellformat.setAlignment(Alignment.CENTRE);
        fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
        fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
        lbl = new Label(5, rolls + 17 + 3, "Re-inspect", fontCellformat);
        sheet2.addCell(lbl);

        fontCell = new WritableFont(WritableFont.TIMES, 11, WritableFont.BOLD);
        fontCellformat = new WritableCellFormat(fontCell);
        fontCellformat.setAlignment(Alignment.CENTRE);
        fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
        fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
        lbl = new Label(11, rolls + 17 + 3, "Nok", fontCellformat);
        sheet2.addCell(lbl);

        fontCell = new WritableFont(WritableFont.TIMES, 11, WritableFont.BOLD);
        fontCellformat = new WritableCellFormat(fontCell);
        fontCellformat.setAlignment(Alignment.CENTRE);
        fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
        fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
        lbl = new Label(47, rolls + 17 + 2, "revised", fontCellformat);
        sheet2.addCell(lbl);

        fontCell = new WritableFont(WritableFont.TIMES, 11, WritableFont.BOLD);
        fontCellformat = new WritableCellFormat(fontCell);
        fontCellformat.setAlignment(Alignment.CENTRE);
        fontCellformat.setVerticalAlignment(VerticalAlignment.CENTRE);
        fontCellformat.setBorder(Border.ALL, BorderLineStyle.THIN);
        lbl = new Label(47, rolls + 17 + 3, "date", fontCellformat);
        sheet2.addCell(lbl);

        copy.write();
        copy.close();
        w.close();

    }

    public void main() throws WriteException, IOException, BiffException {

        inputFile = "F:\\normalPatch2.xls";

        open();

        writeReport();

    }

}
